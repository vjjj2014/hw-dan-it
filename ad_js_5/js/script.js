// Асинхронность(А.), это способность одновременного выполнения нескольких процессов в скрипте, не дожидаясь завершения
// предыдущего. С учетом того, что код в джаваскрипте выполняется линейно и последовательно, А. играет огромную роль при
// распределении задач и надобности выполнения разных кусочков кода.


    async function init () {

        async function getIp () {
            const response = await fetch('https://api.ipify.org/?format=json')
            const data = await response.json()
            return data.ip;
        }

        async function getIpInfo (ip) {
            const response =  await fetch(`http://ip-api.com/json/${ip}`)
            const data = await response.json()
            return data;
        }

        const ip = await getIp();
        const data = await getIpInfo(ip);

        document.body.innerHTML = JSON.stringify(data);
    }
init();
