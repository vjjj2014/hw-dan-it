// Ответы на теоретичесские вопросы:
// 1. Функции придуманы для того, что бы не повторять один и тот же код во многих местах.
//     Функции являются основными «строительными блоками» программы.
//
//     2. Когда объявляется имя аргумента во время объявления функции, то по сути, этим помечается тот позиционный аргумент
// для последующего использования внутри этой функции. Если аргумент не был передан, то метка для него имеет значение undefined.
//     Если передан аргумент, но для него нет позиционной метки, то он существует в области видимости функции, но не имеет имени.
// Иными словами аргумент нужен для расширения возможности функции.


let firstNum = +prompt('first');
let secondNum = +prompt('second');
let operator = prompt('operate');

function calc(first, second, operate) {
    console.log(first);
    console.log(second);
    console.log(operate);
    let result;
    if (operate === '+') {
        result = first + second;
        return result;
    } else if (operate === '-') {
        result = first - second;
        return result;
    } else if (operate === '*') {
        result = first * second;
        return result;
    } else if (operate === '/') {
        result = first / second;
        return result;
    }
}

console.log(
    'Результат выполнения функции',
    calc(firstNum, secondNum, operator)
);