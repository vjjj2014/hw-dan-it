document.addEventListener('DOMContentLoaded',onReady);
function onReady () {
    //inputs
    let firstInput = document.getElementById('name');
    let secondInput = document.getElementById('lastName');
    //spans
    let firstSpan = document.getElementById('firstname_error');
    let secondSpan = document.getElementById('lastname_error');
    //on blur validation
    firstInput.onblur = function () {
        if (firstInput.value.length < 10 ){
            firstSpan.innerHTML = 'Value must be more than 10 symbols'
            firstSpan.style.color = 'red'
        }
    }
    //on blur validation
    secondInput.onblur = function () {
        if (secondInput.value.length < 10){
            secondSpan.innerHTML = 'Value must be more than 10 symbols'
            secondSpan.style.color = 'red'
        }
    }
    //on focus validation
    firstInput.onfocus = function () {
        firstSpan.innerHTML = '';
    }
    //on focus validation
    secondInput.onfocus = function () {
        secondSpan.innerHTML = ''
    }
}