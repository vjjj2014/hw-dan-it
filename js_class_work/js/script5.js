// document.addEventListender('DOMContentLoaded'...)
// window.addEventListender('load'...)
// element.classList.toggle('hidden')
//
// addEventListener(event, handler);

document.addEventListener('DOMContentLoaded', function() {
    const container = document.getElementById('container');

    container.onclick = function() {
        shuffle(container);
    };

    shuffle(container);
});

function getRandomImage() {
    const imgArr = ['https://www.thesprucepets.com/thmb/-UHyBMUvHaMyFuCPWRnoYgLhVgA=/2304x1296/smart/filters:no_upscale()/close-up-of-cat-lying-on-floor-at-home-908763830-1d61bee6961b45ee8a55bdfa5da1ebb3.jpg',

        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTcL1RYzYE3wHmPkT5YklfKkNynvRGgz1E8ww&usqp=CAU',

        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQIjlBaF6PKd5h7C7WzSinmvigDaSZ6nPeOXQ&usqp=CAU',

        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRZR-LoaOFYudrL_R7JIv213nXtjtq66ci-lg&usqp=CAU',

        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR7BaL3LDkbRXyN0YvXonkr7Svw8oz00qaZVw&usqp=CAU',

        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS0mrch-c34yz4QN-eQoEkPaxwSRlEb3eZJRQ&usqp=CAU',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQo4BQSpilYy5KuAptMxbOAxm4uKjFYDG6_wg&usqp=CAU',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTRJgUnizcKoLtrGY-5fhpNLBSI0Cy6N69K2A&usqp=CAU',
        'https://d3544la1u8djza.cloudfront.net/APHI/Blog/2016/10_October/persians/Persian+Cat+Facts+History+Personality+and+Care+_+ASPCA+Pet+Health+Insurance+_+white+Persian+cat+resting+on+a+brown+sofa-min.jpg',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSlNyI5Bbsl1vq1BQjH9XA-Z4j0Kkk0cEpAnA&usqp=CAU',
        'https://www.petfoodindustry.com/ext/resources/Images-by-month-year/19_11/cat-CBD-cannabis-marijuana-leaf.jpg',
        'https://www.sciencenewsforstudents.org/wp-content/uploads/2020/05/1030_LL_domestic_cats-1028x579.jpg'];

    const randomIndex = Math.floor(Math.random() * imgArr.length);

    return imgArr[randomIndex] || 'https://www.atorus.ru/public/ator/data/0a0e4b.jpg';
}


function shuffle(container, numOfShuffle = 6) {
    container.innerHTML = 'loading...';

    setTimeout(function() {

        container.innerHTML = '';
        for(let i = 0; i < numOfShuffle; i++) {
            const img = document.createElement('img');
            img.setAttribute('src', getRandomImage());
            container.append(img);
        }
    }, 500);
}
