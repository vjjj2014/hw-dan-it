//Экранирование используется для поиска(добавления) специальных символов, путем добавления перед ними \. Также существуют и др. способы
//экранирования символов или строк.

function createNewUser() {

    let userBirthday = prompt('Enter your birthday', 'dd.mm.yyyy');
    const firstName = prompt('What is your name?');
    const lastName = prompt(' What is your surname?');


    return user = {
        birthday: userBirthday,
        firstName: firstName,
        lastName: lastName,

        getAge: function () {
            let currentDate = new Date();
            let currentYear = currentDate.getFullYear();
            let userDate = +this.birthday.substring(0, 2);
            let userMouth = +this.birthday.substring(3, 5);
            let userYear = +this.birthday.substring(6, 10);

            let userBirthDate = new Date(userYear, userMouth-1, userDate-1);
            let birthYear = userBirthDate.getFullYear();
            return currentYear - birthYear;
        },

        getPassword: function () {
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6,10);        }
    }
}

console.log(createNewUser());
console.log(user.getAge());
console.log(user.getPassword());