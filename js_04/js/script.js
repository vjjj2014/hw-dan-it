// Методом объекта есть определенная операция над ним, как правило является функцией. Методы - это встроенные в объект функции,
//     которые работают с переменными внутри объекта и передаными ему в качестве аргументов значениями.


function createNewUser() {
    const firstName = prompt('What is your name?');
    const lastName = prompt(' What is your surname?');

    return {
        firstName: firstName,
        lastName: lastName,

        getLogin: function () {
        let str = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
            return str;
        }
    }
}
const user = createNewUser();

console.log(user.getLogin());



