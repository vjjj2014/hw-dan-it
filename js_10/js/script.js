const btn = document.querySelector('.btn');
const firstInput = document.querySelector('.first');
const secondInput = document.querySelector('.second');
btn.addEventListener('click', function (event) {
    event.preventDefault();
    if (firstInput.value === secondInput.value) {
        alert('You are welcome');
    }
    else {
        const error = document.createElement('p');
        error.textContent = 'Нужно ввести одинаковые значения';
        error.style.color = 'red';
        secondInput.after(error);
    }
});
const iconPassword = document.querySelectorAll('.icon-password');
iconPassword.forEach(icon => {
    icon.addEventListener('click', function (event) {
        if (icon.classList.contains('fa-eye')) {
            icon.classList.replace('fa-eye', 'fa-eye-slash')

        }
        else {
            icon.classList.replace('fa-eye-slash', 'fa-eye')
        }
    });
});
