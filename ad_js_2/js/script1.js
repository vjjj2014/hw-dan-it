// try...catch используется для того, что бы отловить исключение. Во-первых, потому что так код более читабельный,
// а обработка ошибок выведется в отдельный блок. Во-вторых, для повышения производительности кода. Как пример, если
// в коде допущена ошибка(при ошибке соединения, при обработке некорректных данных), то не летит вся система, а идет
// переброс на catch(что позволяет всей системе фукнционировать дальше в обычном режиме).

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const ul = document.createElement('ul');

document.querySelector('#root').append(ul);

books.forEach((elem, index) => {
        try {
            if (!elem.author)
                throw new Error('incorect author for index ' + index);
            else if (!elem.name)
                throw new Error('incorect name for index ' + index);
            else if (!elem.price)
                throw new Error('incorect price for index ' + index);

            const li = document.createElement('li');
            li.textContent = elem.name;
            ul.append(li);
        }
        catch (err) {
            console.error(err);
        }
});


