const tabNav = document.querySelector('.tabs');
const allTextContent = document.querySelectorAll('.tab-content');
tabNav.addEventListener('click', function (ev) {
    const tabMenu = document.querySelector('.active');
    tabMenu.classList.remove('active');
    allTextContent.forEach(item => {
        if(item.dataset.text === ev.target.dataset.name){
            item.classList.add('active')
        } else {
            item.classList.remove('active')}
        })
    ev.target.classList.add('active');
});

