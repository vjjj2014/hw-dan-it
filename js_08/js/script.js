//Каждое доступное событие имеет обработчик событий — блок кода, который будет запускаться при срабатывании события.
// Когда такой блок кода определен на запуск в ответ на возникновение события, мы говорим, что мы регистрируем обработчик событий.

const input = document.getElementById('number');
const error = document.getElementById('error');
const price = document.getElementById('price');
input.onfocus = function () {
    input.classList.add('focus');
    if (input.classList.contains('error')) {
        input.classList.remove('error');
        error.textContent = '';
    }
}
input.onblur = function () {
    if (input.value <= 0) {
        input.classList.add('error');
        error.textContent = 'Please enter correct price';
        return;
    }
    let span = document.createElement('span');
    span.textContent = `Текущая цена: ${input.value}`;
    let button = document.createElement('span');
    button.textContent = 'x';
    button.classList.add('button');
    span.appendChild(button);
    price.appendChild(span);
    price.classList.add('price');
    input.style.color = 'green';
    button.addEventListener('click', function () {
        span.hidden = true;
        button.hidden = true;
        price.classList.remove('price');
        input.value = '';
    })
}