//Прототип даёт нам немного «магии». Когда мы хотим прочитать свойство из object, а оно отсутствует, JavaScript
// автоматически берёт его из прототипа. В программировании такой механизм называется «прототипным наследованием».
//Т.е. это  многие интересные возможности языка и техники программирования, которые основываются на нём.

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get name() {
        return this._name;
    }
    get age() {
        return this._age;
    }
    get salary() {
        return this._salary;
    }
    set name(value) {
        this._name = value;
    }
    set age(value) {
        this._age = value;
    }
    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang;
    }
    get salary() {
        return super.salary * 3;
    }
    set salary(value) {
        super.salary = value;
    }
}

let employee = new Programmer('Vova', 26, 32000);
console.log(employee.salary);



