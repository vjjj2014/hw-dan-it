//Деструктуризация – это специальный синтаксис, который позволяет нам «распаковать» массивы
// или объекты в кучу переменных, так как иногда они более удобны.Т.е. при Д.массива мы можем
// использовать переменные вместо элементов массива. При Д. объекта мы получаем список названий переменных в {...}.


const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const allClients = [...clients1, ...clients2]
//1-ый способ
//     const filter = allClients.filter((element, index) => allClients.indexOf(element) == index);
// console.log(filter);

//2-ой способ
const filter = [];
allClients.forEach((element, index) => {
    if (allClients.indexOf(element) == index)
        filter.push(element);
});
console.log(filter);