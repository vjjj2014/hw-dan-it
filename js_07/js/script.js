//Объектная Модель Документа (DOM) – возможность взаимосвязи JS с элементами на странице в виде опреленной структуры.



function showArray(array) {
    const ul = document.createElement('ul');
    let list = array.map((element) => {
        const li = document.createElement('li');
        li.textContent = element;
        ul.appendChild(li);
    });
    return ul;
}
document.body.after(showArray(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']));





