const tabNav = document.querySelector('.tabs');
const allTextContent = document.querySelectorAll('.tab-content');
tabNav.addEventListener('click', function (ev) {
    const tabMenu = document.querySelector('.active');
    tabMenu.classList.remove('active');
    allTextContent.forEach(item => {
        if(item.dataset.text === ev.target.dataset.name){
            item.classList.add('active')
        } else {
            item.classList.remove('active')}
    })
    ev.target.classList.add('active');
});


const btn = document.querySelector('.load-button');
const allImg = Array.from(document.querySelectorAll('.ourwork'));
let counter = 1;
btn.addEventListener('click', () => {
    if (counter === 1) {
        const newArr = allImg.slice(0, 12);
        newArr.forEach(item => {
            item.classList.add('active-more');
        });
        counter++;
    } else {
        const newArr = allImg.slice(12);
        newArr.forEach(item => {
            item.classList.add('active-more');
        });
        counter++;
        btn.remove();
    }
});


const workNav = Array.from(document.querySelectorAll('.raw-picture'));
const tabWork = document.querySelector('.tabs-work');
const tabWorkItem = document.querySelectorAll('.tab-work');
tabWork.addEventListener('click', function (ev) {
    tabWorkItem.forEach(item => {
        item.classList.remove('active-work')
    });
    if(counter === 1) {
        workNav.slice(0,12).forEach(item => {
            if(item.dataset.text === ev.target.dataset.name) {
                return item.classList.add('active')}
            else if (item.dataset.work === ev.target.dataset.name) {
                return item.classList.add('active');
            }
            item.classList.remove('active');
        });
    } else if (counter === 2) {
        workNav.slice(0,24).forEach(item => {
            if(item.dataset.text === ev.target.dataset.name) {
                return item.classList.add('active')}
            else if (item.dataset.work === ev.target.dataset.name) {
                return item.classList.add('active');
            }
            item.classList.remove('active');
            item.classList.remove('active-more');
        });
    } else {
    workNav.forEach(item => {
        if(item.dataset.text === ev.target.dataset.name) {
            return item.classList.add('active')
        }
        else if (item.dataset.work === ev.target.dataset.name) {
            return item.classList.add('active');
        }
        item.classList.remove('active');
        item.classList.remove('active-more');
    });}
    ev.target.classList.add('active-work');
});



const infoNav = document.querySelector('.picture-navigation');
const allPhotoContent = document.querySelectorAll('.info-photo');
infoNav.addEventListener('click', function (ev) {
    const infoMenu = document.querySelector('.active-image');
    infoMenu.classList.remove('active-image');
    allPhotoContent.forEach(item => {
        if(item.dataset.text === ev.target.dataset.name){
            item.classList.add('active')
        } else {
            item.classList.remove('active')}
    })
    ev.target.classList.add('active-image');
});


let imgContent = document.querySelectorAll('.info-photo');
let currentImg = 0;

function nextStep() {
    imgContent[currentImg].classList.remove('active');
    currentImg = (currentImg + 1) % imgContent.length;
    imgContent[currentImg].classList.add('active');
}
const arrowRight = document.querySelector('.slider-arrow-right');
arrowRight.addEventListener('click', () => {
    nextStep();
});

function previosStep() {
    imgContent[currentImg].classList.remove('active');
    if (currentImg === 0) {
        currentImg = imgContent.length;
    }
    currentImg = (currentImg - 1) % imgContent.length;
    imgContent[currentImg].classList.add('active');
}

const arrowLeft = document.querySelector('.slider-arrow-left');
arrowLeft.addEventListener('click', () => {
    previosStep();
});

