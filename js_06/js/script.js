//Цикл forEach  используется для перебора массива. Он для каждого элемента массива вызывает функцию callback.
// К достоинствам forEach относится то, что здесь не нужно объявлять локальные переменные для хранения индекса
// и значения текущего элемента массива, поскольку они автоматически передаются в функцию обратного вызова (колбек)
// в качестве аргументов.

function filterBy (array, data) {
    const result = array.filter((item) => typeof item !== data);
    return result;
}
console.log('массив до фильтрации', ['hello', 'world', 23, '23', null, 4564, NaN, undefined, false]);
console.log('массив после фильтрации',filterBy(
    ['hello', 'world', 23, '23', null, 4564, NaN, undefined, false],
    'string')
);
